Source: python-pyfaidx
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-coverage,
               python3-setuptools,
               python3-setuptools-scm,
               python3-biopython,
               python3-importlib-metadata <!nocheck>,
               python3-numpy <!nocheck>,
               pybuild-plugin-pyproject,
               python3-pytest <!nocheck>,
               samtools <!nocheck>,
               tabix <!nocheck>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/python-pyfaidx
Vcs-Git: https://salsa.debian.org/med-team/python-pyfaidx.git
Homepage: https://github.com/mdshw5/pyfaidx
Rules-Requires-Root: no

Package: python3-pyfaidx
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
Recommends: python3-biopython
Description: efficient random access to fasta subsequences for Python 3
 Samtools provides a function "faidx" (FAsta InDeX), which creates a
 small flat index file ".fai" allowing for fast random access to any
 subsequence in the indexed FASTA file, while loading a minimal amount of
 the file in to memory. This Python module implements pure Python classes
 for indexing, retrieval, and in-place modification of FASTA files using
 a samtools compatible index. The pyfaidx module is API compatible with
 the pygr seqdb module. A command-line script "faidx" is installed
 alongside the pyfaidx module, and facilitates complex manipulation of
 FASTA files without any programming knowledge.
 .
 This package provides the Python 3 modules to access fasta files.

Package: python-pyfaidx-examples
Architecture: all
Depends: ${misc:Depends}
Suggests: python3-pyfaidx
Multi-Arch: foreign
Description: example data for efficient random access to fasta subsequences for Python
 Samtools provides a function "faidx" (FAsta InDeX), which creates a
 small flat index file ".fai" allowing for fast random access to any
 subsequence in the indexed FASTA file, while loading a minimal amount of
 the file in to memory. This Python module implements pure Python classes
 for indexing, retrieval, and in-place modification of FASTA files using
 a samtools compatible index. The pyfaidx module is API compatible with
 the pygr seqdb module. A command-line script "faidx" is installed
 alongside the pyfaidx module, and facilitates complex manipulation of
 FASTA files without any programming knowledge.
 .
 This package provides example data to test the Python modules.
